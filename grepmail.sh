#!/bin/bash

# Prüfen, ob ein Dateiname als Option übergeben wurde
if [ -n "$1" ]; then
    filename="$1"
else
    echo "Bitte geben Sie den Dateinamen ein:"
    read -r filename
fi

# Muster für die Erkennung von E-Mail-Adressen
email_pattern="[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}"

# Suchen nach E-Mail-Adressen in der Datei oder der Standardeingabe
if [ -f "$filename" ]; then
    grep -E -o "$email_pattern" "$filename"
else
    echo "Die angegebene Datei existiert nicht: $filename"
fi
